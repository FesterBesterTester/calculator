//
//  GraphView.h
//  Calculator
//
//  Created by rsalvo on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SplitViewBarButtonItemPresenter.h"

@protocol GraphHelper <NSObject, SplitViewBarButtonItemPresenter>
@required
-(double)getYforX:(double)x;
-(NSString *)getFunctionName;
@end

@interface GraphView : UIView

@property (nonatomic) CGPoint origin;
@property (nonatomic) CGFloat scale;
@property (nonatomic, weak) id <GraphHelper> delegate;
@end


