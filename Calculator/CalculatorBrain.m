//
//  CalculatorBrain.m
//  Calculator
//
//  Created by rsalvo on 5/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CalculatorBrain.h"

@interface CalculatorBrain()
@property (nonatomic, strong) NSMutableArray *programStack;
@end

@implementation CalculatorBrain

@synthesize programStack = _programStack;

- (NSMutableArray *)programStack
{
    if (_programStack == nil) _programStack = [[NSMutableArray alloc] init];
    return _programStack;
}

- (id)program
{
    return [self.programStack copy];
}


+ (NSString *)descriptionOfTopOfStack:(NSMutableArray *)stack
{
    NSString * result = @"";
    
    id topOfStack = [stack lastObject];
    if (topOfStack) [stack removeLastObject];
        
    if (![self isOperator:topOfStack])
    {
        result = [topOfStack stringValue];
    }
    else
    {
        NSString *numOperands = [[NSString alloc] initWithFormat:@"%@", [self operandCount: topOfStack]];
        
        switch ([numOperands intValue])
        {
            case 0:
                result = [result stringByAppendingFormat:@"%@ ", topOfStack];
                break;
            case 1:
                result = [result stringByAppendingFormat:@"%@(%@)", topOfStack, [self descriptionOfTopOfStack:stack]];
                break;
            case 2:
                {
                    NSString *op1 = [self descriptionOfTopOfStack:stack];
                    NSString *op2 = [self descriptionOfTopOfStack:stack];
                    result = [result stringByAppendingFormat:@"(%@ %@ %@)", op2, topOfStack, op1];
                    break;
                }
        }
    }
    return result;
}


+ (NSString *)descriptionOfProgram:(id)program
{
    NSMutableArray *stack;
    NSString *result = @"";
    
    if ([program isKindOfClass:[NSArray class]]) {
        stack = [program mutableCopy];
    }
    
    while ([stack lastObject])
    {
        result = [result stringByAppendingFormat:@"%@", [self descriptionOfTopOfStack:stack]];
        if ([stack lastObject]) result = [result stringByAppendingString:@", "];
    }
    return result;
}

+ (NSSet *)variablesUsedInProgram:(id)program
{
    NSMutableArray *stack;
    NSSet *vars = [[NSSet alloc] initWithObjects:@"X", nil];
    NSSet *result = [NSSet set];

    if ([program isKindOfClass:[NSArray class]]) {
        stack = [program mutableCopy];
    }
    
    for (NSString *var in vars)
    {
        if ([stack containsObject:var])
        {
            result = [result setByAddingObject:var];
        }
    }
    return [result count] ? result : nil;
}

- (void)pushOperand:(double)operand
{
    [self.programStack addObject:[NSNumber numberWithDouble:operand]];
}

- (void)pushOperator:(NSString *)operation
{
    [self.programStack addObject:operation];
}

+ (double)popOperandOffProgramStack:(NSMutableArray *)stack
{
    double result = 0;

    id topOfStack = [stack lastObject];
    if (topOfStack) [stack removeLastObject];

    if ([topOfStack isKindOfClass:[NSNumber class]])
    {
        result = [topOfStack doubleValue];
    }
    else if ([topOfStack isKindOfClass:[NSString class]])
    {
        NSString *operation = topOfStack;
        if ([operation isEqualToString:@"+"])
        {
            result = [self popOperandOffProgramStack:stack] +
                     [self popOperandOffProgramStack:stack];
        }
        else if ([operation isEqualToString:@"*"])
        {
            result = [self popOperandOffProgramStack:stack] *
                     [self popOperandOffProgramStack:stack];
        }
        else if ([operation isEqualToString:@"-"])
        {
            double subtrahend = [self popOperandOffProgramStack:stack];
            result = [self popOperandOffProgramStack:stack] - subtrahend;
        }
        else if ([operation isEqualToString:@"/"])
        {
            double divisor = [self popOperandOffProgramStack:stack];
            if (divisor)
            {
                result = [self popOperandOffProgramStack:stack] / divisor;
            }
        }
        else if ([operation isEqualToString:@"sin"])
        {
            result = sin([self popOperandOffProgramStack:stack]);
        }
        else if ([operation isEqualToString:@"cos"])
        {
            result = cos([self popOperandOffProgramStack:stack]);
        }
        else if ([operation isEqualToString:@"√"])
        {
            double operand = [self popOperandOffProgramStack:stack];
            if (operand >= 0)
            {
                result = sqrt(operand);
            }
        }
        else if ([operation isEqualToString:@"∏"])
        {
            result = 3.14159265;
        }
    }

    return result;
}

+ (double)runProgram:(id)program
{
    NSMutableArray *stack;
    if ([program isKindOfClass:[NSArray class]]) {
        stack = [program mutableCopy];
    }
    return [self popOperandOffProgramStack:stack];
}

+ (double)runProgram:(id)program usingVariableValues:(NSDictionary*)variableMap
{
    NSMutableArray *stack;
    if ([program isKindOfClass:[NSArray class]]) {
        stack = [program mutableCopy];
    }
        
    for (NSUInteger i = 0; i < [stack count]; i++)
    {
        if ([[stack objectAtIndex:i] isKindOfClass:[NSString class]])
        {
           NSString *variable;
            
            for (variable in variableMap)
            {
                if ([[stack objectAtIndex:i] isEqualToString:variable])
                {
                    [stack replaceObjectAtIndex:i withObject:[variableMap objectForKey:variable]];
                    break;
                }
            }
        }
    }
    return [self runProgram:stack];
}

- (void)clearProgram
{
    while ([_programStack lastObject])
    {
        [_programStack removeLastObject];
    }
}

- (void)undo
{
    if ([_programStack lastObject])
    {
        [_programStack removeLastObject];
    }
}

+ (BOOL)isOperator:(NSString *)val
{
    NSSet *operators = [[NSSet alloc] initWithObjects:@"+", @"-", @"/", @"*", @"√",@"∏", @"sin", @"cos", @"X", nil];

    return [operators containsObject:val];
}

+ (NSNumber*)operandCount:(NSString *)operator
{
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:
                          [NSNumber numberWithInt:2], @"+",
                          [NSNumber numberWithInt:2], @"-",
                          [NSNumber numberWithInt:2], @"/",
                          [NSNumber numberWithInt:2], @"*",
                          [NSNumber numberWithInt:1], @"√",
                          [NSNumber numberWithInt:0], @"∏",
                          [NSNumber numberWithInt:1], @"sin",
                          [NSNumber numberWithInt:1], @"cos",
                          [NSNumber numberWithInt:0], @"X",
                          nil];

    return [dict objectForKey:operator];
}

@end
