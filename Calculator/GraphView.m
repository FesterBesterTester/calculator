//
//  GraphView.m
//  Calculator
//
//  Created by rsalvo on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GraphView.h"
#import "AxesDrawer.h"


@implementation GraphView

@synthesize delegate = _delegate;

- (void)setOrigin:(CGPoint)origin
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // we use a string so that a lookup will return nil rather than 0 if no
    // value has previously been stored.
    [defaults setObject:[NSString stringWithFormat:@"%g", origin.x] forKey:@"originX"];
    [defaults setObject:[NSString stringWithFormat:@"%g", origin.y] forKey:@"originY"];
    [defaults synchronize];
}

- (CGPoint)origin
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    CGPoint point;

    // we use a string so that a lookup will return nil rather than 0 if no
    // value has previously been stored.

    if ([defaults stringForKey:@"originX"])
    {
        point.x = [[defaults stringForKey:@"originX"] floatValue];
    }
    else
    {
        point.x = self.bounds.size.width / 2;
    }
    if ([defaults stringForKey:@"originY"])
    {
        point.y = [[defaults stringForKey:@"originY"] floatValue];
    }
    else
    {
        point.y = self.bounds.size.height / 2;
    }
    return point;
}

- (void)setScale:(CGFloat)scale
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // we use a string so that a lookup will return nil rather than 0 if no
    // value has previously been stored.
    [defaults setObject:[NSString stringWithFormat:@"%g", scale] forKey:@"scale"];
    [defaults synchronize];
}

- (CGFloat)scale
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // we use a string so that a lookup will return nil rather than 0 if no
    // value has previously been stored.
    
    if ([defaults stringForKey:@"scale"])
    {
        return [[defaults stringForKey:@"scale"] floatValue];
    }
    else
    {
        return 1;
    }
}

- (void)setup
{    
    self.contentMode = UIViewContentModeRedraw;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self)
    {
        [self setup];
    }
    return self;
}



- (void)setDelegate:(id<GraphHelper>)delegate
{
    if ([delegate conformsToProtocol:@protocol(GraphHelper)])
    {
        _delegate = delegate;
    }
}

- (void)drawFunctionInRect:(CGRect)bounds originAtPoint:(CGPoint)axisOrigin scale:(CGFloat)pointsPerUnit
{
	if (!pointsPerUnit) return;
    	
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextBeginPath(context);
	
    BOOL started = NO;
    CGPoint screenPoint;

    CGFloat onePixel = 1.0 / self.contentScaleFactor;
    
	for (screenPoint.x = 0; screenPoint.x < bounds.size.width; screenPoint.x += onePixel)
	{
        // the input X coordinate in the math function's coordinate space
        // rather than the screen's coordinate space
 		double functionX = (screenPoint.x - axisOrigin.x) / pointsPerUnit;

        // the output Y coordinate in the screen's coordinate space
        // rather than the math function's coordinate space
        screenPoint.y = axisOrigin.y + [self.delegate getYforX:functionX] * -pointsPerUnit;
            
		if (CGRectContainsPoint(bounds, screenPoint))
        {
            if (!started)
            {
                started = YES;
			    CGContextMoveToPoint(context, screenPoint.x, screenPoint.y);
            }
            else
            {
                CGContextAddLineToPoint(context, screenPoint.x, screenPoint.y);
            }
		}
        else
        {
            if (started)
            {
                CGContextStrokePath(context);
                started = NO;
            }
        }
	}
	
	CGContextStrokePath(context);
}

- (void)pan:(UIPanGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateChanged ||
        recognizer.state == UIGestureRecognizerStateEnded)
    {
        CGPoint translation = [recognizer translationInView:self];
        self.origin = CGPointMake(self.origin.x + translation.x, self.origin.y + translation.y);
        [recognizer setTranslation:CGPointZero inView:self];
    }
    
    [self setNeedsDisplay];
}

- (void)pinch:(UIPinchGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateChanged ||
        recognizer.state == UIGestureRecognizerStateEnded)
    {
        self.scale = self.scale * recognizer.scale;
        recognizer.scale = 1;
    }
    
    [self setNeedsDisplay];
}

- (void)tripleTap:(UITapGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded)
    {
        self.origin = [recognizer locationInView:self];
    }
    
    [self setNeedsDisplay];
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGFloat size = self.bounds.size.width;
    NSString *functionDescription = [_delegate getFunctionName];

    if (self.bounds.size.height < self.bounds.size.width) size = self.bounds.size.height;
    
    [AxesDrawer drawAxesInRect:self.bounds originAtPoint:self.origin scale:self.scale];
    
    [self drawFunctionInRect:self.bounds originAtPoint:self.origin scale:self.scale];

    CGPoint textPosition;
    textPosition.x = 5;
    textPosition.y = self.bounds.size.height - 20;

    [AxesDrawer drawString:functionDescription atPoint:textPosition withAnchor:ANCHOR_LEFT];
}

@end
