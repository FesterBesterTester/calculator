//
//  GraphViewController.h
//  Calculator
//
//  Created by rsalvo on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraphView.h"
#import "SplitViewBarButtonItemPresenter.h"

@protocol FunctionHelper <NSObject>
@required
-(id)getProgram;
-(double)calculateYforX:(double)x usingProgram:(id)program;
+(NSString*)getDescription:(id)program;
@end

@interface GraphViewController : UIViewController <GraphHelper, SplitViewBarButtonItemPresenter>
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet GraphView *graph;
@property (weak, nonatomic) id <FunctionHelper>delegate;
-(void)needsUpdate; // should be called when the program needs to be updated

@end
