//
//  GraphViewController.m
//  Calculator
//
//  Created by rsalvo on 6/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GraphViewController.h"

@interface GraphViewController ()
@property id program;
@end


@implementation GraphViewController
@synthesize splitViewBarButtonItem = _splitViewBarButtonItem;
@synthesize toolbar = _toolbar;
@synthesize graph;
@synthesize program = _program;
@synthesize delegate = _delegate;

-(void)needsUpdate
{
    _program = [_delegate getProgram];
    [self.graph setNeedsDisplay];
}

- (void)setDelegate:(id<FunctionHelper>)delegate
{
    if ([delegate conformsToProtocol:@protocol(FunctionHelper)])
    {
        _delegate = delegate;
    }
}

- (void)setSplitViewBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    if (_splitViewBarButtonItem != barButtonItem)
    {
        UIToolbar *toolbar = [self toolbar];
        
        NSMutableArray *toolbarItems = [toolbar.items mutableCopy];
        
        if (_splitViewBarButtonItem)
        {
            [toolbarItems removeObject:_splitViewBarButtonItem];
        }

        if (barButtonItem)
        {
            [toolbarItems insertObject:barButtonItem atIndex:0];
        }
        toolbar.items = toolbarItems;
        _splitViewBarButtonItem = barButtonItem;
    }
}


-(double)getYforX:(double)x
{
    return [_delegate calculateYforX:x usingProgram:_program];
}

-(NSString *)getFunctionName
{
    return [[_delegate class] getDescription:_program];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _program = [_delegate getProgram];
    if ([self.view isKindOfClass:[GraphView class]])
    {
        self.graph = (GraphView*)self.view;
    }
    self.graph.delegate = self;
    [self setPannableView:self.graph];
    [self setPinchableView:self.graph];
    [self setTripleTappableView:self.graph];
}

- (void)viewDidUnload
{
    [self setGraph:nil];
    [self setToolbar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)setPannableView:(UIView *)pannableView
{
    UIPanGestureRecognizer *pangr = [[UIPanGestureRecognizer alloc] initWithTarget:pannableView action:@selector(pan:)];
    [pannableView addGestureRecognizer:pangr];
}

- (void)setPinchableView:(UIView *)pinchableView
{
    UIPinchGestureRecognizer *pinchgr = [[UIPinchGestureRecognizer alloc] initWithTarget:pinchableView action:@selector(pinch:)];
    [pinchableView addGestureRecognizer:pinchgr];
}

- (void)setTripleTappableView:(UIView *)tappableView
{
    UITapGestureRecognizer *tapgr = [[UITapGestureRecognizer alloc] initWithTarget:tappableView action:@selector(tripleTap:)];
    tapgr.numberOfTapsRequired = 3;
    [tappableView addGestureRecognizer:tapgr];
}

/* From "Supporting Orientation Changes in a Split View" in IOS 5.1 Library
   documentation:
     A split view controller relies on its two contained view controllers to
     determine what orientation are supported. It only supports an orientation
     if both contained view controllers do. Even if one of the contained view
     controllers isn’t currently being displayed, it must support the
     orientation.
 
   The GraphView wasn't rotating properly in a split view's detail view until
   both the master and detail view controllers had an implementation of
   shouldAutorotateToInterfaceOrientation.
 */
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
