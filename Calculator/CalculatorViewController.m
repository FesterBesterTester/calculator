//
//  CalculatorViewController.m
//  Calculator
//
//  Created by rsalvo on 5/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CalculatorViewController.h"
#import "CalculatorBrain.h"

@interface CalculatorViewController ()
@property (nonatomic, strong) CalculatorBrain *brain;
@property (nonatomic) BOOL userIsEnteringANumber;
@end

@implementation CalculatorViewController

@synthesize display;
@synthesize history = _history;
@synthesize brain = _brain;
@synthesize userIsEnteringANumber = _userIsEnteringANumber;

- (id <SplitViewBarButtonItemPresenter>) splitViewBarButtonItemPresenter
{
    id detailVC = [self.splitViewController.viewControllers lastObject];
    if (![detailVC conformsToProtocol:@protocol(SplitViewBarButtonItemPresenter)])
    {
        detailVC = nil;
    }
    return detailVC;
}

-(id)getProgram
{
    return self.brain.program;
}

-(double)calculateYforX:(double)x usingProgram:(id)program
{
    NSDictionary *variables = [NSDictionary dictionaryWithObject:[NSNumber numberWithDouble:x] forKey:@"X"];
    return [CalculatorBrain runProgram:program usingVariableValues:variables];
}

+(NSString*)getDescription:(id)program
{
   return [CalculatorBrain descriptionOfProgram:program];
}

- (CalculatorBrain *)brain
{
   if (!_brain)
   {
       _brain = [[CalculatorBrain alloc] init];
   }
   return _brain;
}

- (IBAction)digitPressed:(UIButton *)sender
{
    [self updateUI:[sender currentTitle]];
}

- (IBAction)enterPressed
{
    [self.brain pushOperand: [self.display.text doubleValue]];
    self.userIsEnteringANumber = NO;
    [self updateUI:@""];
}

- (IBAction)clearPressed
{
    // This will clear all entries in the program and
    // operation stack
    [self.brain clearProgram];

    self.userIsEnteringANumber = NO;
    [self updateUI:@""];
}

- (IBAction)undoPressed
{
    if (self.userIsEnteringANumber)
    {
        if (self.display.text.length == 1)
        {
            self.userIsEnteringANumber = NO;
        }
        else
        {
            self.display.text = [self.display.text substringToIndex:self.display.text.length - 1];
        }
    }
    else
    {
        [self.brain undo];
    }
    [self updateUI:@""];
}

- (GraphViewController *)splitViewGraphViewController
{
    GraphViewController *gvc;

    if (self.splitViewController)
    {
        gvc = [self.splitViewController.viewControllers lastObject];
        if (![gvc isKindOfClass:[GraphViewController class]])
        {
            gvc = nil;
        }
    }
    return gvc;
}

- (IBAction)graphPressed
{
    if (self.splitViewController)
    {
        [self.splitViewGraphViewController needsUpdate];
    }
    else
    {
        [self performSegueWithIdentifier:@"showGraph" sender:self];
    }
}

- (IBAction)operationPressed:(UIButton *)sender
{
    if (self.userIsEnteringANumber)
    {
        [self enterPressed];
    }
    [self.brain pushOperator:sender.currentTitle];

    self.userIsEnteringANumber = NO;
    [self updateUI:@""];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showGraph"])
    {
        GraphViewController *newController = segue.destinationViewController;
        
        newController.delegate = self;
    }
}

- (void)updateUI:(NSString *)withDigit
{
    NSRange pointLocation = [self.display.text rangeOfString:@"."];
    BOOL point = [withDigit isEqualToString:@"."];

    if (self.userIsEnteringANumber)
    {
        if (point && pointLocation.length)
        {
            // user has entered multiple decimal points. Ignore
        }
        else
        {
            self.display.text = [self.display.text stringByAppendingString:withDigit];
        }
    }
    else if (![withDigit isEqualToString:@""])
    {
        if (point)
        {
            self.display.text = @"0.";
        }
        else
        {
            self.display.text = withDigit;
        }
        self.userIsEnteringANumber = YES;
    }

    // if user is not entering a number, update the display to show the results
    // of the current calculation
    if (!self.userIsEnteringANumber)
    {
        self.display.text = [NSString stringWithFormat:@"%g", [CalculatorBrain runProgram:self.brain.program]];
    }

    self.history.text = [CalculatorBrain descriptionOfProgram:self.brain.program];
}

/* From "Supporting Orientation Changes in a Split View" in IOS 5.1 Library
 documentation:
 A split view controller relies on its two contained view controllers to
 determine what orientation are supported. It only supports an orientation
 if both contained view controllers do. Even if one of the contained view
 controllers isn’t currently being displayed, it must support the
 orientation.
 
 The GraphView wasn't rotating properly in a split view's detail view until
 both the master and detail view controllers had an implementation of
 shouldAutorotateToInterfaceOrientation.
 */
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (self.splitViewController)
    {
        return YES;
    }
    else
    {
        return UIInterfaceOrientationIsPortrait(interfaceOrientation);
    }
}

- (BOOL)splitViewController:(UISplitViewController *)svc
   shouldHideViewController:(UIViewController *)vc 
              inOrientation:(UIInterfaceOrientation)orientation
{
    return [self splitViewBarButtonItemPresenter] ? UIInterfaceOrientationIsPortrait(orientation) : NO;    
}

- (void)splitViewController:(UISplitViewController *)svc
     willHideViewController:(UIViewController *)aViewController
          withBarButtonItem:(UIBarButtonItem *)barButtonItem
       forPopoverController:(UIPopoverController *)pc
{
    barButtonItem.title = self.title;
    [self splitViewBarButtonItemPresenter].splitViewBarButtonItem = barButtonItem;
}

- (void)splitViewController:(UISplitViewController *)svc
     willShowViewController:(UIViewController *)aViewController
  invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    [self splitViewBarButtonItemPresenter].splitViewBarButtonItem = nil;
}

- (void)viewDidLoad
{
    if (self.splitViewController)
    {
        self.splitViewController.delegate = self;
    }

    if (self.splitViewGraphViewController)
    {
        self.splitViewGraphViewController.delegate = self;
    }
    
    // swiping left and right to make the popover appear/disappear interferes
    // with dragging to scroll the graph view, so disable presentation via
    // swipe gesture
    self.splitViewController.presentsWithGesture = NO;
}

- (void)viewDidUnload {
    [super viewDidUnload];
}
@end
