//
//  CalculatorViewController.h
//  Calculator
//
//  Created by rsalvo on 5/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GraphViewController.h"

@interface CalculatorViewController : UIViewController <FunctionHelper, UISplitViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *display;
@property (weak, nonatomic) IBOutlet UILabel *history;
@end
